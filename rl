#!/usr/bin/env sh
rg --smart-case --pretty --sort-files --max-columns 500 "$@" | less -RFX
